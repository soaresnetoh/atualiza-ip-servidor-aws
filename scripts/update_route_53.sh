#!/bin/bash

DOMINIO=$1
SUBDOMINIO=$2
IP=$3

var=$(docker image ls | grep '^route53_update ' | cut -c 1-15)
if [[ $var != "route53_update " ]]; then
    git clone https://gitlab.com/soaresnetoh/atualizar-ip-no-route53.git  > /dev/null 2>&1 
    docker build ./atualizar-ip-no-route53 -t route53_update > /dev/null 2>&1 
    rm -Rf ./atualizar-ip-no-route53 > /dev/null 2>&1 
fi

# Atualizando o route53
docker run --rm -v $PWD/.credentials:/root/.aws/credentials route53_update $DOMINIO $SUBDOMINIO $IP

#echo -e "docker run --rm -v $PWD/.credentials:/root/.aws/credentials route53_update $DOMINIO $SUBDOMINIO $IP"
