#!/bin/bash

ID_INSTANCIA=$1
REGIAO=$2
contador=1

pegaip () {
    ip_instancia=$(docker run --rm -v $PWD/.credentials:/root/.aws/credentials amazon/aws-cli ec2 \
                describe-instances \
                --region $REGIAO \
                --filters \
                "Name=instance-state-name,Values=running" \
                "Name=instance-id,Values=$ID_INSTANCIA" \
                --query 'Reservations[*].Instances[*].[PublicIpAddress]' \
                --output text)
}

verifica_state () {
    instancia_state=$(docker run --rm -v $PWD/.credentials:/root/.aws/credentials amazon/aws-cli ec2 \
                           describe-instances --filters \
                           "Name=tag:Name,Values='[Ramper] Jenkins'" \
                           --query "Reservations[].Instances[].State[].Name" \
                           --region us-east-2 \
                           --output text)
}

start_jenkins () {
    instancia_on=$(docker run --rm -v $PWD/.credentials:/root/.aws/credentials amazon/aws-cli ec2 \
            start-instances \
            --region $REGIAO \
            --instance-ids $ID_INSTANCIA)
}

# Função mensagem
mensagem () {
  echo "Tentativa n. " $contador
}

verifica_state
case $instancia_state in
   "stopped") 
        # echo "Instancia stoped ... iniciando...."
        start_jenkins
        contador=60
        while [[ $contador -gt 0 ]];
        do
            # if [[ $contador -eq 60 ]] | [[ $contador -eq 40 ]] | [[ $contador -eq 20 ]] | [[ $contador -eq 3 ]] | [[ $contador -eq 2 ]] | [[ $contador -eq 1 ]]; then
            #     echo -e "Aguarde: " $contador "segundos\n"
            # fi
            contador=$[ contador - 1 ]
            sleep 1
        done
        pegaip
    ;;
   "running") 
        # echo "Instancia running ... pegando IP...."
        pegaip
    ;;
   *)
        echo "Instancia não esta em estado stopped nem running, tente novamente em alguns minutos ou verifique na console"
        exit 1
    ;;
esac


if [[ -z $ip_instancia ]]; then
    #mensagem $contador
    while [[ $contador -le 3 ]];
    do
        if [[ -n $ip_instancia ]]; 
        then
            contador=5
        else
            #mensagem $contador
            #echo "Esperando 3 segundos para testar novamente"
            start_jenkins
            sleep 3
            pegaip

        fi
        contador=$[ contador + 1 ]
    done


    if [[ $contador -eq 4 ]] | [[ -z $ip_instancia ]]; then
        #echo 5 - "Nao Iniciou a instancia ou instancia não tem ip publico"
        echo "ERRO_IP"
        exit 0
    fi

    if [[ -z $ip_instancia ]]; then
    #mensagem $contador
    contador=1
    while [[ $contador -le 3 ]];
    do
        if [[ -z $ip_instancia ]];then
            sleep 5
            pegaip
            contador=$[ contador + 1 ]
        else
            contador=4
        fi
        
    done
    fi
 else
     echo $ip_instancia
    #  echo 7 - "NAO VAZIO"
fi 
