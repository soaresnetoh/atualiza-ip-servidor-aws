# ATUALIZA IP SERVIDOR AWS

Este projeto visa iniciar uma instancia e atualizar seu ip no DNS Route53 sem a necessidade de logar na console AWS.

Para concluir a atualizaço do IP usa-se internamente  o conteúdo do repositório  
https://gitlab.com/soaresnetoh/atualizar-ip-no-route53.git

-----
### Modo de uso
-----

Seu usuario precisa ter acesso a :  
***EC2 - Ler e Dar Start em uma instancia  
Route53 - Ler e Atualizar***



**Após o clone**

crie um arquivo chamado **dados.txt** e coloque o conteudo abaixo, atualizando com sua realidade:

```
### Atualize estas variáeis
DOMINIO="seudominio.com.br"
SUBDOMINIO="seu_app"
ID_INSTANCIA="i-06xxxxxxxxxxxxx7f"
REGIAO="us-east-2"
AWS_ACCESS_KEY_ID="ABCDEFGHIJKLM7IJT7PID"
AWS_SECRET_ACCESS_KEY="SxJK9n9VhnjhNUuyGf6gv&guü6gRtKtqWX9kAWIhHBJdWL"
```

No projeto clonado, voce terá script shell para fazer o procedimento de atualização no Route53. Ele precisa estar com direito de execução :   

```
chmod +x atualiza-ip.sh
./atualiza-ip.sh
```