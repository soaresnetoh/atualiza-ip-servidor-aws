#!/bin/bash

parada () {
        read -p "Parada.. continua o script? - (S)im ou (N)ão?" yn
        case $yn in
                [YySs]* ) 
                        break
                ;;
                [Nn]* ) 
                        
                        exit 0
                ;;
                * ) echo "Responda (S)im ou (N)ão.";;
        esac
}

### Atualize estas variáeis
source dados.txt

echo -e "[default]\naws_access_key_id = $AWS_ACCESS_KEY_ID\naws_secret_access_key = $AWS_SECRET_ACCESS_KEY" > .credentials

echo `date` - "Pegando IP da Instancia --- Aguarde no maximo 5 minutos!"
resultado=`./scripts/pega-ip-jenkins.sh $ID_INSTANCIA $REGIAO`


if [[ $resultado = "ERRO_IP" ]]; then
        echo "Nao Iniciou a instancia ou não tem ip publico"
        exit 0
fi

if [[ $resultado = "Instancia não esta em estado stopped nem running, tente novamente em alguns minutos ou verifique na console" ]]; then
        echo "Instancia não esta em estado stopped nem running, tente novamente em alguns minutos ou verifique na console"
        exit 0
fi


IP=$resultado

echo -e "\n"`date` - "Continuando script com o ip " $resultado "\n"

novo=`./scripts/update_route_53.sh $DOMINIO $SUBDOMINIO $IP`

echo -e `date` "\n"$novo - " Ciclo Completo."
