
REGIAO="us-east-2"
ID_INSTANCIA=$(docker run --rm -v $PWD/.credentials:/root/.aws/credentials amazon/aws-cli ec2 \
                           describe-instances \
                           --filters "Name=tag:Name,Values='[Ramper] Jenkins'" \
                           --query "Reservations[].Instances[].InstanceId" \
                           --region $REGIAO \
                           --output text)

docker run --rm -v $PWD/.credentials:/root/.aws/credentials amazon/aws-cli ec2 \
                           stop-instances \
                           --instance-ids $ID_INSTANCIA \
                           --region $REGIAO --output text